# NSW COVID-19 case tracker

This is a simple R shiny + leaflet application to track local cases. 

# data source

nsw covid 19 cases:
https://data.nsw.gov.au/data/dataset/covid-19-cases-by-location/resource/21304414-1ff1-4243-a5d2-f52778048b29?inner_span=True

AU Postcode:

https://github.com/matthewproctor/australianpostcodes


# Exmaple
https://adouzzy.shinyapps.io/covid19/